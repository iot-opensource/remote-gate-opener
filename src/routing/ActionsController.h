#ifndef __ACTIONS_CONTROLLER_H__
#define __ACTIONS_CONTROLLER_H__

#ifndef Arduino_h
#include <Arduino.h>
#endif
#include <ESPAsyncWebServer.h>
#include <ArduinoJson.h>
#include <Ticker.h>
#include "../models/Gate.h"
#include "consts.h"

class ActionsController
{
    Gate *gate;
    Ticker *timer;

public:
    ActionsController(Gate *gate, Ticker *timer);
    ~ActionsController();

    void openGateAction(AsyncWebServerRequest *request);
    void closeGateAction(AsyncWebServerRequest *request);
    void stopGateAction(AsyncWebServerRequest *request);
};

#endif