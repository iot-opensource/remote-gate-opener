#include <Arduino.h>
#include <SPI.h>
#include <ESP8266WiFi.h>
#include <Hash.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <AsyncElegantOTA.h>
#include <OneWire.h>
#include <DS18B20.h>
#include <Ticker.h>
#include "../include/consts.h"
#include "routing/DefaultController.h"
#include "routing/ActionsController.h"
#include "models/Gate.h"
#include "models/InsideThermometer.h"

Gate *gate;
AsyncWebServer *httpServer;
DefaultController *defaultController;
ActionsController *actionsController;
InsideThermometer *insideThermometer;
DS18B20 *outsideThermometer;
OneWire oneWire(PIN_THERMOMETER_OUTDOOR);
Ticker *timer;

const uint8_t logsLimit = 100;
std::vector<String> logs;

void stopGate()
{
    gate->stop();
}

void log(String message)
{
    Serial.println(message);

    if (logs.size() >= 100) {
        logs.erase(logs.begin());
    }

    logs.push_back(message);
}

void notFound(AsyncWebServerRequest *request)
{
    request->send(404, "text/plain", "Not found");
}

void setup()
{
    Serial.begin(9600);

    gate = new Gate(PIN_OPEN, PIN_CLOSE);
    httpServer = new AsyncWebServer(HTTP_SERVER_PORT);
    outsideThermometer = new DS18B20(&oneWire);
    insideThermometer = new InsideThermometer(PIN_THERMOMETER_INDOOR);
    timer = new Ticker(stopGate, OPEN_TIME_MS, MILLIS);
    defaultController = new DefaultController(gate, outsideThermometer, insideThermometer, timer);
    actionsController = new ActionsController(gate, timer);

    log("Remote gate opener IoT");

    WiFi.mode(WIFI_STA);
    WiFi.begin(STASSID, STAPSK);

    log("Wait for WiFi connection");

    if (WiFi.waitForConnectResult() != WL_CONNECTED)
    {
        Serial.printf("WiFi Failed!\n");
        return;
    }

    log("WiFi connected!");

    httpServer->on("/", [](AsyncWebServerRequest *request)
                   { defaultController->mainRoute(request); });
    httpServer->on("/api/v1/gate", HTTP_GET, [](AsyncWebServerRequest *request)
                   { defaultController->gateStatus(request); });
    httpServer->on("/api/v1/temperature-outside", HTTP_GET, [](AsyncWebServerRequest *request)
                   { defaultController->temperatureOutside(request); });
    httpServer->on("/api/v1/temperature-inside", HTTP_GET, [](AsyncWebServerRequest *request)
                   { defaultController->temperatureInside(request); });
    httpServer->on("/api/v1/logs", HTTP_GET, [](AsyncWebServerRequest *request)
                   {
                        DynamicJsonDocument doc(4000);
                        JsonArray array = doc.to<JsonArray>();

                        for (unsigned int i = 0; i < logs.size(); i++) {
                            array.add(logs[i]);
                        }

                        String json;
                        serializeJson(doc, json);

                        request->send(200, "application/json", json);
                    });

    httpServer->on("/api/v1/actions/open", HTTP_POST, [](AsyncWebServerRequest *request)
                    { 
                       actionsController->openGateAction(request); 
                       log("Opened by API");
                    });
    httpServer->on("/api/v1/actions/close", HTTP_POST, [](AsyncWebServerRequest *request)
                    { 
                        actionsController->closeGateAction(request);
                        log("Closed by API");
                    });
    httpServer->on("/api/v1/actions/stop", HTTP_POST, [](AsyncWebServerRequest *request)
                    { 
                        actionsController->stopGateAction(request); 
                        log("Stopped by API");
                    });

    httpServer->begin();

    if (outsideThermometer->begin() == false)
    {
        log("ERROR: Outside thermometer not found");
    }

    pinMode(BUTTON_OPEN, INPUT);
    pinMode(BUTTON_CLOSE, INPUT);
    pinMode(BUTTON_STOP, INPUT);

    httpServer->onNotFound(notFound);
    AsyncElegantOTA.begin(httpServer);
    httpServer->begin();
}

void loop()
{
    if (digitalRead(BUTTON_OPEN) == HIGH && gate->getState() != GATE_OPEN && gate->getState() != GATE_OPENING)
    {
        delay(50);

        timer->start();
        gate->open();

        log("Opened by button or radio");

        while(digitalRead(BUTTON_OPEN) == HIGH);
    }

    if (digitalRead(BUTTON_CLOSE) == HIGH && gate->getState() != GATE_CLOSE && gate->getState() != GATE_CLOSING)
    {
        delay(50);

        timer->start();
        gate->close();

        log("Closed by button or radio");

        while(digitalRead(BUTTON_CLOSE) == HIGH);
    }

    if (digitalRead(BUTTON_STOP) == HIGH)
    {
        delay(50);

        timer->stop();
        gate->stop();

        log("Stopped by button or radio");

        while(digitalRead(BUTTON_STOP) == HIGH);
    }

    if (timer->state() == RUNNING)
    {
        timer->update();
    }
}