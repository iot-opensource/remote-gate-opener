#ifndef __GATE_H__
#define __GATE_H__

#include <Arduino.h>

enum State
{
    GATE_OPEN = 10,
    GATE_OPENING = 11,
    GATE_CLOSE = 20,
    GATE_CLOSING = 21,
    GATE_STOPPED = 0
};

class Gate
{
	uint8_t openPin;
    uint8_t closePin;
    State state;

public:
	Gate(uint8_t openPin, uint8_t closePin);
	~Gate();
    void open();
    void close();
    void stop();
    String stateAsString();
    State getState();
};

#endif //__GATE_H__
