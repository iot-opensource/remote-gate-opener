#ifndef __InsideThermometer_H__
#define __InsideThermometer_H__

#include <Arduino.h>

class InsideThermometer
{
	uint8_t analogPin;

public:
	InsideThermometer(uint8_t analogPin);
	~InsideThermometer();
    float temperature();
};

#endif //__InsideThermometer_H__
